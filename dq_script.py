


def customer_site(data,customer_site_label):
    working_df = data
    site = []
    customer_site_id = customer_site_label
    customer_site = working_df.groupby([customer_site_id]).size()
    customer_site = customer_site.reset_index()     
        
    # customer site heuristic
    if len(customer_site) < 2:
        site += [customer_site[customer_site_id][0]]
    else:
        for i in customer_site[customer_site_id]:
            site += [i]
        
    return site


def time_check(data,time_label):
    df = data
    timestamp = time_label
    
    df["day_month"] = df[timestamp].dt.date
    min_date = min(df['day_month'])
    max_date = max(df['day_month'])
    num_of_days = len(df['day_month'].unique())

            
    return min_date, max_date


def service_net(data):
    df = data
    service_id = df.groupby(['service_network_id']).size()
    service_id = service_id.reset_index()

    if len(service_id) < 2:
        serv_net_id = service_id['service_network_id'][0]
            
    else:
        serv_net_id = []
        for i in service_id['service_network_id']:
            serv_net_id += [i]
    
    return serv_net_id



def x_check(data,x_value):
    if x_value is None:
        pass
    
    else:
        working_df = data
        x_value = str(x_value)
    
    # Looking at metrics for the x values
    
        x_mean = working_df['total_unit_x'].mean()
        x_median = working_df['total_unit_x'].median()
        x_std = working_df['total_unit_x'].std()
        x_min = working_df['total_unit_x'].min()
        x_max = working_df['total_unit_x'].max()
        x_25th = working_df['total_unit_x'].quantile(.25)
        x_75th = working_df['total_unit_x'].quantile(.75)
        

    return x_mean, x_median, x_std, x_min, x_max, x_25th, x_75th



def y_check(data,y_value):
    working_df = data
    stats = []
    y_value = str(y_value)
    
    if y_value is None:
        pass
    else:
        
        y_mean = working_df[y_value].mean()

        y_std = working_df[y_value].astype(float).std()
        y_min = working_df[y_value].min()
        y_max = working_df[y_value].max()
        y_25th = working_df[y_value].astype(float).quantile(.25)
        y_median = working_df[y_value].median()
        y_75th = working_df[y_value].astype(float).quantile(.75)
    
    return y_mean,y_std,y_min,y_max,y_25th,y_median,y_75th


def cycle_identifier(data,y):
    df = data
    df = df[['timestamp',y]]
    df['prev_y'] = df[y].shift()
    df['prev_y'].iloc[0] = 0.0
    #print(df)
    cycle = 0
    cycle_id = []
    for index, row in df.iterrows():
            
        if row[y] <= 0.0:
            cycle_id += [np.nan]
        
        elif (row[y] > 0.0) & (row['prev_y'] == 0.0):
            cycle += 1
            cycle_id += [cycle]
    
        elif (row[y] > 0.0) & (row['prev_y'] > 0.0):
            cycle_id += [cycle]
            
        else:
            cycle_id += [np.nan]
        
    
    df['cycle_id'] = cycle_id
    
    return df



def cycle_stats(data,y):
    df = cycle_identifier(data,y)
    
    cycle_stats = df.groupby('cycle_id').size()
    cycle_stats = df.reset_index()

    stats = []
    
    for i in cycle_stats['cycle_id']:
        sub_df = cycle_df.loc[cycle_df['cycle_id']==i]
        cycle_count = len(sub_df)
        cycle_min = sub_df['appliance_watt_mins'].min()
        cycle_max = sub_df['appliance_watt_mins'].max()
        cycle_median = sub_df['appliance_watt_mins'].median()
        cycle_mean = sub_df['appliance_watt_mins'].mean()
        cycle_std = sub_df["appliance_watt_mins"].astype(int).std()
        
        sub_stats = [cycle_count, cycle_min,cycle_max,cycle_median,cycle_mean,cycle_std]
        stats += [sub_stats]
    
    #return stats
    return cycle_count,cycle_min,cycle_max,cycle_median,cycle_mean,cycle_std



    